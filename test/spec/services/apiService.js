

describe('Unit test: alerts', function () {
  var alerts;
  var $rootScope;
  // Load your module.
  beforeEach(module('robotApp'));

  beforeEach(inject(function(_$rootScope_, _alerts_) {
    // The injector unwraps the underscores (_) from around the parameter names when matching
    alerts = _alerts_;
    $rootScope = _$rootScope_;
  }));

  it('can get an instance of factory', function() {
    expect(alerts).toBeDefined();
  });


  it('should test add method', function() {
    var type = "info";
    var msg = "Successfully updated";
    var autoClose = true;

    alerts.add(type,msg, autoClose);
    expect($rootScope.alerts).toBeDefined();
    expect($rootScope.alerts).toEqual([{'type': type, 'msg': msg, duration: 2000}]);

    type = "alert";
    alerts.add(type,msg, autoClose);
    expect($rootScope.alerts).toEqual([{'type': "info", 'msg': msg, duration: 2000}, {'type': "alert", 'msg': msg, duration: 2000}]);
  });


  it('should test closeAlert method', function() {
    var type = "info";
    var msg = "Successfully updated";
    var autoClose = true;

    alerts.add(type,msg, autoClose);
    expect($rootScope.alerts).toEqual([{'type': type, 'msg': msg, duration: 2000}]);

    alerts.closeAlert(0);
    expect($rootScope.alerts).toEqual([]);

  });

});
