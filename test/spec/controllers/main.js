'use strict';

describe('Controller: MainCtrl', function() {

  var $rootScope;
  var $scope;
  var $compile;
  var ctrl;
  var apiService;
  var alerts;

  // load the controller's module
  beforeEach(module('robotApp'));

  beforeEach(inject(function(_$rootScope_, _$compile_, $controller, _apiService_, _alerts_) {
    // The injector unwraps the underscores (_) from around the parameter names when matching
    alerts = _alerts_;
    apiService = _apiService_;
    $scope = _$rootScope_.$new();
    $rootScope = _$rootScope_;
    $compile = _$compile_;

    ctrl = $controller("MainCtrl", {
      $scope: $scope
    });

  }));

  describe('$scope.robot', function() {

    it('sets the strength to "strong" if the password length is >8 chars', function() {
      expect($scope.robot.name).toEqual('');
    });

    it('should test robot placing', function() {
      $scope.robot = {
        name: "Test Robot",
        direction: "LEFT",
        coordX: 2,
        coordY: 0
      };

      spyOn(apiService, 'placeRobot').and.callThrough();
      $scope.placeRobot();
      expect(apiService.placeRobot).toHaveBeenCalled();
      expect(apiService.placeRobot).toHaveBeenCalledWith('Test Robot', {
        angle: 'LEFT',
        x_pos: 2,
        y_pos: 0
      });
    });

    it('should test createRobot', function() {
      $scope.robot = {
        name: "Test Robot"
      };

      spyOn(apiService, 'createRobot').and.callThrough();
      $scope.createRobot();
      expect(apiService.createRobot).toHaveBeenCalled();
      expect(apiService.createRobot).toHaveBeenCalledWith('Test Robot');
    });

    it('should test getReport', function() {
      $scope.robot = {
        name: "Test Robot"
      };

      spyOn(apiService, 'getReport').and.callThrough();
      $scope.getReport();
      expect(apiService.getReport).toHaveBeenCalled();
      expect(apiService.getReport).toHaveBeenCalledWith('Test Robot');
    });

    it('should test execAction', function() {
      $scope.robot = {
        name: "Test Robot"
      };
      var command = "move";

      spyOn(apiService, 'execAction').and.callThrough();
      $scope.execAction(command);
      expect(apiService.execAction).toHaveBeenCalled();
      expect(apiService.execAction).toHaveBeenCalledWith('Test Robot', command);
    });

    it('should test alerts', function() {
      var type = "info";
      var msg = "Successfully updated";
      var autoClose = true;

      spyOn(alerts, 'add').and.callThrough();
      $scope.showAlert(type,msg, autoClose);
      expect(alerts.add).toHaveBeenCalled();
      expect(alerts.add).toHaveBeenCalledWith(type,msg, autoClose);
    });

  });
});
