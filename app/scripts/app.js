'use strict';

/**
 * @ngdoc overview
 * @name robotApp
 * @description
 * # robotApp
 *
 * Main module of the application.
 */
angular
  .module('robotApp', [
    'ui.bootstrap',
    'dialogs.main'
  ]);
