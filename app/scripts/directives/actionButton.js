'use strict';

/**
 * @ngdoc function
 * @name robotApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the robotApp
 */
angular.module('robotApp')
  .directive('actionButton', function () {
    return {
      restrict: 'A',
      link: function(scope, el, attrs) {
        el.bind('click', function() {
          scope.execAction(attrs.action);
        });
      }
    };
  });
