(function() {
  'use strict';

  angular.module('dialogs', [])
    .provider('dialogs', function () {
      var modalDefaults =  {
        backdrop: true,
        keyboard: true,
        windowCLass: 'dialogs-default',
        backdropClass: 'dialogs-backdrop-default',
        size: 'md',
        templateUrl: '/views/confirm.html',
        animation: true
      };

      return {
        $get: ['$modal', function ($modal) {
          return {
            confirm: function (customModalDefaults, modalOptions) {
              var tempModalDefaults = {};
              var tempModalOptions = {};

              angular.extend(tempModalDefaults, modalDefaults, customModalDefaults);
              angular.extend(tempModalOptions, modalOptions);

              if (!tempModalDefaults.controller) {
                tempModalDefaults.controller = function ($scope, $modalInstance) {
                  $scope.opts = tempModalOptions.data;
                  $scope.ok = function (result) {
                    tempModalOptions.confirmCallback();
                    $modalInstance.close(result);

                  };
                  $scope.close = function () {
                    tempModalOptions.cancelCallback();
                    $modalInstance.dismiss('cancel');
                  };
                };
              }
              return $modal.open(tempModalDefaults);
            }
          };
        }]
      };
    });
})();
