'use strict';

angular.module('robotApp')
  .factory('apiService', ['$http', function($http) {
    var baseUrl = 'http://toyrobotservice-env.elasticbeanstalk.com/webapi/robot/';
    return {
      createRobot: function(name) {
        return $http.post(baseUrl.concat(name));
      },
      placeRobot: function(name, data) {
        return $http({
          url: baseUrl.concat(name + '/position'),
          method: "POST",
          data: data,
          headers: {
            "Content-Type": "application/json"
          }
        });
      },
      getReport: function(name) {
        return $http.get(baseUrl + name + '/position');
      },
      execAction: function(name, command) {
        return $http.put(baseUrl + name + '/position/' + command);
      }
    };
  }])
.factory('alerts', function($rootScope) {
  var alertService = {};
  var alertCloseTime = 2000;
  $rootScope.alerts = [];

  alertService.add = function(type, msg, autoclose, duration) {
    duration = duration || alertCloseTime;
    $rootScope.alerts.push({'type': type, 'msg': msg, duration: autoclose ? duration : 'none'});
  };

  alertService.closeAlert = function(index) {
    $rootScope.alerts.splice(index, 1);
  };

  return alertService;
});
