'use strict';

/**
 * @ngdoc function
 * @name robotApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the robotApp
 */
angular.module('robotApp')
  .controller('MainCtrl', ['$scope', '$rootScope', '$window', 'alerts', 'dialogs', 'apiService', function ($scope, $rootScope, $window, alerts, dialogs, apiService) {
    $scope.robot = {
      name : '',
      isNameSet: false,
      coordX: 0,
      coordY: 0,
      direction: 'north',
      report: ''
    };


    var createSuccessCallback = function (response) {
        $scope.showAlert('success', 'Robot has been created!', true);
        $scope.robot.name = response.data;
        showReport('Robot ' + response.data + ' has been created');
    };



    var robotPlacementError = function () {
      showConfirmModal('error', 'Please place your robot');
    };

    var serviceError = function() {
      showConfirmModal('error', 'Sorry, service api does not respond');
    };


    var showConfirmModal = function (msg) {
      var dlg = dialogs.confirm('Please confirm', msg, {size:'md'});
      dlg.result.then(function(){
        showReport('Robot ' + $scope.robot.name + ' has been chosen.');
        $scope.robot.isNameSet = true;
      },function(){
        $scope.robot.name = '';
        $scope.robot.isNameSet = false;
      });
    };

    var serviceErrorCallback = function (xhr) {
      var msg = 'Ooops. Robot ' + $scope.robot.name + ' already exists. Proceed?';
      switch(xhr.status) {
        case 303: showConfirmModal(msg); break;
        case 204: robotPlacementError(); break;
        default: serviceError(); break;
      }
    };

    $rootScope.closeAlert = alerts.closeAlert;

    var showReport = function(text) {
      $scope.robot.report = $scope.robot.report.concat(text + '\n');
    };

    /**
     * Create robot with entered name
     */
    $scope.createRobot = function () {
      apiService.createRobot($scope.robot.name).
        then(createSuccessCallback, serviceErrorCallback);
    };

    $scope.showAlert = function(type, msg, autoClose) {
      alerts.add(type, msg, autoClose);
    };

    $scope.placeRobot = function() {
      var data = { angle : $scope.robot.direction.toUpperCase(), x_pos : $scope.robot.coordX ,y_pos:$scope.robot.coordY};
      apiService.placeRobot($scope.robot.name, data).
        then(function(response) {
          var res = response.data;
          showReport('Robot was placed: x=' + res.x_pos + ' y=' + res.y_pos + ' angle=' + res.angle);
        }, function(xhr) {
          serviceErrorCallback(xhr);
        });
    };

    $scope.getReport = function() {
      apiService.getReport($scope.robot.name).
        then(function (response) {
          var res = response.data;
          showReport('Robot coordinates: x=' + res.x_pos + ' y=' + res.y_pos + ' angle=' + res.angle);
        }, serviceErrorCallback);
    };

    /**
     * Execute robot command
     * @param command {String} command to be executed
     */
    $scope.execAction = function (command) {
      apiService.execAction($scope.robot.name, command).
        then(function() {
          showReport('Robot action: ' + command);
        }, function() {
          serviceErrorCallback();
        });
    };
  }]);
